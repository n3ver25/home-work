const express = require('express')
const bookRouter= express.Router() 
const app = express()
const bodyParser= require('body-parser')


app.use(bodyParser.json()) 
app.use(bodyParser.urlencoded({ extended: true }))


var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;

MongoClient.connect('mongodb://n3ver25:5931305059@ds052837.mlab.com:52837/mfu059', (err, db) => {
  if (err) return console.log(err)

  app.listen(3000, () => {
    console.log('app working on 3000')
  });

  let dbase = db.db("mfu059");

  app.post('/mfu', (req, res, next) => {

    let mfu = {
      id: req.body.id,
     school: req.body.school,
      member: req.body.member,
    };//สร้าง

    dbase.collection("mfu").save(mfu, (err, result) => {
      if (err) {
        console.log(err);
      }

      res.send('mfu added successfully');
    });

  });

  app.get('/mfu', (req, res, next) => {
    dbase.collection('mfu').find().toArray((err, results) => {
      res.send(results)
    });//เรียก
  });

  app.get('/mfu/:id', (req, res, next) => {
    if (err) {
      throw err;
    }//เรียกตามid

    let id = ObjectID(req.params.id);
    dbase.collection('mfu').find(id).toArray((err, result) => {
      if (err) {
        throw err;
      }

      res.send(result);
    });
  });

  app.put('/mfu/:id', (req, res, next) => {
    var id = {
      _id: new ObjectID(req.params.id)
    };//update

    dbase.collection("mfu").update(id, { $set: { 
      id: req.body.id,
     school: req.body.school,
      member: req.body.member
 } }, (err, result) => {
      if (err) {
        throw err;
      }

      res.send('user updated sucessfully');
    });
  });


  app.delete('/mfu/:id', (req, res, next) => {
    let id = ObjectID(req.params.id);
        //delete
    dbase.collection('mfu').deleteOne({ _id: id }, (err, result) => {
      if (err) {
        throw err;
      }

      res.send('mfu deleted');
    });
  });
});    